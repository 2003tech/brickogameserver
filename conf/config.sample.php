<?php
$host = '127.0.0.1';
$db   = 'blocklandium';
$user = '';
$pass = '';

$basepath = '/';

$tplCache = 'templates/cache';
$tplNoCache = true; // **DO NOT SET AS TRUE IN PROD - DEV ONLY**

$forumEnabled = false;

// Should dark mode be default?
$darkModeDefault = false;

// Redirect all non-internal pages to https.
$https = false;

// Cookie token name. Don't change this too often as it'll invalidate old logins!
$cookieName = 'token';

// Website domain.
$domain = 'https://example.org';

// Discord server link. If blank will disable Discord link.
$invite = '';

// URL to Discord webhook for new level uploads. Leave blank to disable this.
$webhook = '';

$lpp = 20;