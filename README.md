# **Brickogame Server**
The website and APIs for Brickogame. This will be also used as the auth server for the game eventually.

## Disclaimer

**Both the Server and Game are WIP, and are in Pre-Alpha stages. Some code is stll missing.**

Compared to previous Gamerappa projects (2003page and squareBracket/cheeseRox), we're using Gitlab instead of GitHub.

## Contributors

**Gamerappa**
Developer of Brickogame, and it's website/API's

**TheBirdWasHere/Grumpz**
Maintainer of repository, alpha/beta tester.
