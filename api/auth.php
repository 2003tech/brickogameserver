<?php
chdir('../');
$rawOutputRequired = true;
require('lib/common.php');

$key = htmlspecialchars($_GET["key"]);
$logindata = fetch("SELECT id, name FROM users WHERE authKey = ?", [$key]);

if(isset($logindata["id"]))
{
	header("Status: SUCCESS");
	header("BGID: " . $logindata["id"]);
	header("NAME: " . $logindata["name"]);
}
else 
{
	header("Status: FAILURE");
}
echo("Return Message"); //you need this for brickogame client to disconnect